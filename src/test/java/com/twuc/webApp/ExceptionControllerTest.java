package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ExceptionControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    void should_throw_error_code_when_runtime_exception() throws Exception {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/runtime-exception", String.class);
        assertEquals(HttpStatus.NOT_FOUND, forEntity.getStatusCode());
        assertEquals("handle runtime exception", forEntity.getBody());
    }

    @Test
    void should_throw_error_code_when_null_point_exception() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/errors/null-pointer", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntity.getStatusCode());
        assertEquals("Something wrong with the argument", forEntity.getBody());
    }

    @Test
    void should_throw_error_code_when_arithmetic_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/arithmetic", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_throw_error_when_brother_throw_illegal_argument_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with brother or sister.", entity.getBody());
    }

    @Test
    void should_throw_error_when_sister_throw_illegal_argument_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with brother or sister.", entity.getBody());
    }
}

