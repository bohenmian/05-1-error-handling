package com.twuc.webApp.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThrowableController {

    @GetMapping("/api/brother-errors/illegal-argument")
    public ResponseEntity illegalArgumentException() {
        throw new IllegalArgumentException();
    }

}
