package com.twuc.webApp.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.AccessControlException;

@RestController
@ControllerAdvice
@RequestMapping("/api")
public class ExceptionController {

    @GetMapping("/exceptions/runtime-exception")
    public ResponseEntity<String> runtimeException() {
        throw new RuntimeException();
    }

    @GetMapping("/exceptions/access-control-exception")
    public ResponseEntity<String> accessControlException() {
        throw new AccessControlException("this is access control exception");
    }

    @GetMapping("/errors/null-pointer")
    public ResponseEntity<String> nullPointerException() {
        throw new NullPointerException();
    }

    @GetMapping("/errors/arithmetic")
    public ResponseEntity<String> arithmeticException() {
        throw new NullPointerException();
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> handleNullPointerAndArithmeticException() {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .body("Something wrong with the argument");
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException() {

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("handle runtime exception");
    }

    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<String> handleAccessControlException() {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body("handle access control exception");
    }
}
